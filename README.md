# lazy_news
This repo contains python code written and maintained by me, ThomasAndreatta for downloading and formatting news from various website cause I'm lazy and i have no time even for retributed working

## To Do
### NewsSites
Add the other news Sources
* Hackster: https://www.hackster.io/
* Adafruits: https://blog.adafruit.com/
* Hackaday: https://hackaday.com/blog/
* MagpiRaspberry: https://magpi.raspberrypi.org/articles
* sparkFunk: https://www.sparkfun.com/news
* dangerousPrototypes: http://dangerousprototypes.com/blog/
* instructables: https://www.instructables.com/circuits/arduino/projects/?page=1
* makenzine: https://makezine.com/projects/
* seedStudio: http://www.seeedstudio.com/blog/tag/<seedStudio_tag>


### Implemented website:
* arduinoBlog: https://blog.arduino.cc/





## DONE
### No duplicate
* remove the news already published from my 'coworker'

### Interface
Create something for showing the news and in some way implement a decision process for choosing which news elaborate and which throw into the trashcan
  Ideas:
  * Show a numbered list with the news and once one get chosen the news on the screen from 0 to chosen_number gets 'deleted' from the view and discarded and only the chosen_number to last_one still on the screen. The chosen one will be saved into a file and later elaborated(command line, looks always cool)

### Save and Download
* save the chosen news and download the related pic
* write the choosen news into a new file and download the image associated(maybe putting everything in a different folder for
  every news)

## Other basic things


## Notes
### MainStructure
* retrive news from all the website and add them into news_list
* remove coworker news with "retrive.remove_coworker(list)"
* remove the previously published news (datafile)
* show interface
* create translated newslist (maybe)
* save the chosen news into a folder per news and download the image associated
* save into a file the chosen news (datafile)



### seedStudio tags for url composition
seedStudio_tags =['AI','ARDUINO','NANO','INDUSTRIAL-IOT','IOT','LORAWAN','MACHINE-LEARNING','MAKER','MAKER-FAIRE','NVIDIA','NANO','OPEN-HARDWARE','PROJECT','RASPBERRY-PI','RASPBERRY-PI-4','ROBOTICS','SENSOR','SHIELD']
