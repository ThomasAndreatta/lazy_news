#!/usr/bin/python3
from .tprint import *
from .news import *
from .retrive_class import *
import requests
from bs4 import BeautifulSoup, SoupStrainer
import urllib
import re
import pprint as pprint
from google_trans_new import google_translator

class dev:

    def main(self,param):
        import os
        import shutil
        with tprint("Downloading, translating and saving all the selected news.","All the news have been downloaded, translated and saved. ✓"):
            retobj =retrive()
            

            folderPath = os.getcwd()+'/newsFolder'
            shutil.rmtree(folderPath,ignore_errors=True)
            os.mkdir(folderPath)


            for key in param:
                fullpath=folderPath+'/'+key
                os.mkdir(fullpath)
                fileName=fullpath+'/'+key+'.txt'
                with open(fileName,'w+') as f:
                    f.write(param[key]["content"])
                    f.write("\n\n==========\n\n")
                    f.write(self.translate(param[key]["content"])+'\n')
                    f.write('Origin: '+param[key]["origin"]+'\n')
                    f.write("\nlinks:\n")
                    i=0
                    for link in param[key]['links']:
                        imgPath= fullpath+'/'+str(i)+'.'+link.split('.')[len(link.split('.'))-1]
                        f.write(link+'\n')
                        retobj.download_image(link,imgPath)
                        i=i+1


    def translate(self,content):
        translator = google_translator()
        #you can specify the translate languege
        result = translator.translate(content, lang_src='en', lang_tgt='it')
        return result










# download news developing part
    def get_site(self,param):
        news_list = {}
        set = self.get_link(1)#min and max are for debug, just remove them and set 0, 10

        '''for url in set:
            tmp = self.get_news(url)
            news_list[tmp.title] = {
            'origin' : tmp.origin,
            "content":tmp.content,
            'links':tmp.pics
            }
        return news_list'''


    def get_link(self,max):#return an ARRAY of NEWS all the different news link from the post page of arduinoBlog
        lista_link = set()
        tprint.info("Sto scaricando i link negro")
        for _ in range(0, int(max)):
            url ='https://www.pcbway.com/project/shareproject/{}.html?sort=scorerec'.format(_)
            resp = requests.get(url)
            print(resp.status_code)
            '''parser = 'lxml'  # or 'lxml' (preferred) or 'html5lib', if installed
            if(resp.status_code == 200):
                soup = BeautifulSoup(resp.text, parser)
                for x in soup.find_all("article", {"class": "post"}):
                    for link in x.find_all('a', href=True):
                        if('https://blog.arduino.cc/category/' not in link['href']):
                            lista_link.add(str(link['href']))
            else:
                break'''

    def get_news(self,url):#return a NEWS ELEMENT(tmp.field) with the actual news given an url of arduinoBlog
        tmp = news()
        tmp.origin = url
        base_soup = BeautifulSoup(requests.get(url).text, 'lxml')

        new_soup = BeautifulSoup(str(base_soup.find_all("div", {"class":"entry"})).split('<section class="bottom-article">')[0][1:], "html.parser")#extract only the text of the element inside of the entry class, the [1:] cut away a random brackets


        tmp.title = base_soup.find_all("h3", {"class":"post-title"})[0].text
        content_text =new_soup.text.strip()


        content_text = re.sub(r'^https?:\/\/.*[\r\n]*', '', content_text, flags=re.MULTILINE)#remove all links from the content
        tmp.content = content_text

        for img in new_soup.findAll('img'):
            tmp.pics.append(img.get('src'))
        return tmp
