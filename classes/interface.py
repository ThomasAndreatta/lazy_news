#!/usr/bin/python3
#control the interface and return the selected news, escape char "q"
from .tprint import *
import os,platform
from sty import fg, bg, ef, rs

class interface:

    def start_interface(self,news_list,round):#if round 0 show only the title, if != print the title and the content for the final selection
        #tp = tprint()
        chosen_news={}
        numbered_news =[]#arrray of news that have to be choosen(needed for the int index)

        for key in news_list:#append in numbered news
            numbered_news.append(key)

        while True:
            if(round == 0):
                self.show_interface(numbered_news)#show news
            else:
                if(len(news_list) != 0):
                    self.show_interface2(numbered_news,news_list)#show news
                else:
                    tprint.info("No news selected. Ending.")
                    input("")
                    break

            inp = input('>')
            isnum,inp = self.checkint(inp,len(numbered_news))#check if its a valid number or not
            if(isnum == False):
                if inp == 'q':#escape char
                    if(round == 0):
                        tprint.info("Ok. Moving on the second selection.")

                    else:
                        tprint.info("The news have been checked and saved.")
                    input("")
                    break
                else:
                    tprint.error("Not a valid input!")
                    input("")
            else:#valid input
                chosen_news[numbered_news[inp]] =news_list[numbered_news[inp]]#add the selected news to the news_list
                numbered_news = numbered_news[inp+1:]#remove all the news from 0 to selected index
            if(len(numbered_news)==0):#no more news
                if(round == 0):
                    tprint.info("All the news have been checked. Moving on the second selection.")
                else:
                    tprint.info("All the news have been checked and saved.")
                input("")
                break
        return chosen_news

    def show_interface(self,ls):#show the news list to choose from

        self.clearConsole()
        tprint.system("q) exit")
        for x in range(0,len(ls)):
            print("{}) {}".format(x,ls[x]))

    def show_interface2(self,ls,news):#show the news list to choose from

        self.clearConsole()
        tprint.system("q) exit\n==========")
        for x in range(0,len(ls)):
            print("{}) {}:\n{}".format(fg.blue + str(x)+fg.rs,fg.red+ls[x]+fg.rs,news[ls[x]]['content']))
            tprint.info('==========')




    def checkint(self,inp,max):#check if int and in range for the interface
        if(inp.isdigit()):
            if(int(inp) >= 0 and int(inp) <max):
                return True, int(inp)
            else:
                return False, inp
        else:
            return False, inp


    def clearConsole(self):
        if(platform.os == "Windows"):
            os.system('cls')
        else:
            os.system('clear')
        self.banner()




    def banner(self):
        import random
        from art import tprint
        #font = ["block","broadway","colossal","doom"] #legit font to use
        print('\n')
        tprint("- Lazy News -",font="colossal")
