#!/usr/bin/python3
#control the interaction with the json datafile, write and read
import json


class jsonInteraction:

    def write_news(self,lista,datafile):#given a news type DICTIONARY of object save them as json structure into the datafile
        data ={}
        
        for x in lista:
            tmp={
                'origin' : lista[x]["origin"],
                "content":lista[x]["content"],
                'links':lista[x]["links"]
                }

            data[x]=tmp

        with open( datafile , "w" ) as file:
            json.dump( data , file,indent=4)

    def read_news(self,datafile,num):#read all the news from the datafile into news_list DICTIONARY and return it
        news_list={}
        f = open(datafile)
        data = json.load(f)
        f.close()

        for key in data:#iterate trough every main node into the json file, access to the subnode with data[key]['subnode']
            if(num == 0):
                break
            if(num >0):
                #print('Title: {}\nContent:\n{}\nOrigin: {}\nPic Links:'.format(key,data[key]['content'],data[key]['origin']))
                news_list[key]={
                'origin' : data[key]['origin'],
                "content":data[key]['content'],
                'links':data[key]['links']
                }
                num=num-1
            else:
                news_list[key]={
                'origin' : data[key]['origin'],
                "content":data[key]['content'],
                'links':data[key]['links']
                }

        return news_list
