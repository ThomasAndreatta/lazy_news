#!/usr/bin/python3
from .news import *
import requests
from .tprint import *
from bs4 import BeautifulSoup, SoupStrainer
import urllib
import re
import gspread
from oauth2client.service_account import ServiceAccountCredentials


class retrive:
    #ArduinoBlog part

    def create_adruino(self,last_page):#return a DICTIONARY of news type element with the arduinoBlog news, last_page have to be an int > 0
        news_list = {}
        set = self.get_link_arduino(0,last_page)#min and max are for debug, just remove them and set 0, 10

        for url in set:
            tmp = self.get_news_arduino(url)
            news_list[tmp.title] = {
            'origin' : tmp.origin,
            "content":tmp.content,
            'links':tmp.links
            }
        return news_list

    def get_link_arduino(self,min,max):#return an ARRAY of NEWS all the different news link from the post page of arduinoBlog
        lista_link_arduino = set()
        
        for _ in range(min, max):
            url ='https://blog.arduino.cc/2021/page/{}/'.format(_)
            parser = 'lxml'  # or 'lxml' (preferred) or 'html5lib', if installed
            resp = requests.get(url)
            if(resp.status_code == 200):
                soup = BeautifulSoup(resp.text, parser)
                for x in soup.find_all("article", {"class": "post"}):
                    for link in x.find_all('a', href=True):
                        if('https://blog.arduino.cc/category/' not in link['href']):
                            lista_link_arduino.add(str(link['href']))
            else:
                break


        return lista_link_arduino

    def get_news_arduino(self,url):#return a NEWS ELEMENT(tmp.field) with the actual news given an url of arduinoBlog
        tmp = news()
        tmp.origin = url
        base_soup = BeautifulSoup(requests.get(url).text, 'lxml')

        new_soup = BeautifulSoup(str(base_soup.find_all("div", {"class":"entry"})).split('<section class="bottom-article">')[0][1:], "html.parser")#extract only the text of the element inside of the entry class, the [1:] cut away a random brackets


        tmp.title = base_soup.find_all("h3", {"class":"post-title"})[0].text
        content_text =new_soup.text.strip()


        content_text = re.sub(r'^https?:\/\/.*[\r\n]*', '', content_text, flags=re.MULTILINE)#remove all links from the content
        tmp.content = content_text

        for img in new_soup.findAll('img'):
            tmp.links.append(img.get('src'))
        return tmp

    #End of arduinoBlog part

    #Global method

    def get_coworker(self):#return an ARRAY of the coworker news links

        link_list=[] #list of all the coworker news
        link_regex = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', re.DOTALL)#regex for spotting links

        with open('./confFile/sheet_url.conf','r') as f:#the googlesheets links
            sheet_data = f.readlines()

        credentials = ServiceAccountCredentials.from_json_keyfile_name('./confFile/service-account.conf', 'https://www.googleapis.com/auth/spreadsheets')#apidata for service-account
        gc = gspread.authorize(credentials)#set credentials

        sht1 = gc.open_by_url(sheet_data[0])#open the googlesheet via link
        worksheet = sht1.worksheet(sheet_data[1].rstrip())#schoose which page(remove the new line char)

        for rows in worksheet.get_all_values():#get a list of list as RowsxCols
            for col in rows:
                if(len(re.findall(link_regex, col)) > 0):#if it's not an empty set add it to the url list
                    link_list.append(re.findall(link_regex, col)[0][0])#get only the url in the list
        link_list = list(dict.fromkeys(link_list))#remove the double news
        return link_list

    def download_image(self,url,title):#download an image given a url and a title(with ext)
        urllib.request.urlretrieve(url, "{}".format(title))

    def remove_coworker(self,news_list):#remove all the coworker news from the given list of news(as dictionary)
        #coworker_newslist = self.get_coworker()
        debug_coworkerlist = ['https://blog.arduino.cc/2021/04/02/april-6th-scheduled-maintenance-planned-for-the-arduino-website/']

        remove_list = []
        for coworker_news in debug_coworkerlist:
            for key in news_list:#iterate trough every main node into the json file, access to the subnode with data[key]['subnode']
                if(news_list[key]['origin'] == coworker_news):
                    remove_list.append(key)

        for x in remove_list:
            news_list.pop(x)

        return news_list
