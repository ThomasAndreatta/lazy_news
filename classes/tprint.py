#!/usr/bin/python3
#print message with colorss
from sty import fg, bg, ef, rs
from itertools import cycle
from shutil import get_terminal_size
from threading import Thread
from time import sleep

class tprint:
    def error(content):
        print(fg.red + content +fg.rs)


    def info(content):
        print(fg.yellow + content +fg.rs)

    def system(content):
        print(fg.blue + content +fg.rs)

#============== from here down there's the loading text animation, how to use is at the end of the file

    def __init__(self, desc="Loading...", end="Done!", timeout=0.1):
        """
        A loader-like context manager

        Args:
            desc (str, optional): The loader's description. Defaults to "Loading...".
            end (str, optional): Final print. Defaults to "Done!".
            timeout (float, optional): Sleep time between prints. Defaults to 0.1.
        """
        self.desc = desc
        self.end = end
        self.timeout = timeout

        self._thread = Thread(target=self._animate, daemon=True)
        self.steps = ["⢿", "⣻", "⣽", "⣾", "⣷", "⣯", "⣟", "⡿"]
        self.done = False

    def start(self):
        self._thread.start()
        return self

    def _animate(self):
        for c in cycle(self.steps):
            if self.done:
                break
            print(fg.yellow +f"\r{self.desc} {c}"+fg.rs, flush=True, end="")
            sleep(self.timeout)

    def __enter__(self):
        self.start()

    def stop(self):
        self.done = True
        cols = get_terminal_size((80, 20)).columns
        print("\r" + " " * cols, end="", flush=True)
        print(fg.blue +f"\r{self.end}"+fg.rs, flush=True)

    def __exit__(self, exc_type, exc_value, tb):
        # handle exceptions with those variables ^
        self.stop()


'''with Loader("work in progress text","work is ended text"):
        print("at the end of these operations it will end by itself")

    loader = Loader("Loading with object...", "That was fast!", 0.05).start()
    for i in range(10):
        sleep(0.25)
    loader.stop()'''
