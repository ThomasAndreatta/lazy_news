#!/usr/bin/python3
from classes import *
from classes.tprint import tprint

import os,platform
import copy


'''
retobj method:
create_adruino(last_page): #return a list of news type element with the arduinoBlog news, last_page have to be an int > 0
remove_coworker(self,news_list):#remove all the coworker news from the given list of news(as dictionary)
download_image(self,url,title):#download an image given a url and a title(with ext)
'''

retobj =retrive_class.retrive() #it's just a global var for avoiding the creation of a retrive_class object in every method cause i'm lazy as fuck
interface = interface.interface()
ji = jsonInteraction.jsonInteraction()
dev = dev.dev()


datafile = 'jsonFile/datafile.json'  #the file where all the good news get stored
news_list = {} #all the news from the different website
chosen_news = {} #list of the news to publish



def debug_news(data):#print some element of a news DICTIONARY for checking everything is ok
    tprint.info('+++++++++++ DEBUG +++++++++++++')
    for key in data:#iterate trough every main node into the json file, access to the subnode with data[key]['subnode']
        #print('Title: {}\nContent:\n{}\nOrigin: {}\nPic Links:'.format(key,data[key]['content'],data[key]['origin']))
        print('Title: {}'.format(key))


def arduinoBlog():#return a DICTIONARY of the arduinoBlog news AND add them into news_list
    global news_list
    with tprint("Arduino Blog>","Arduino Blog> ✓"):
        tmp_list = retobj.create_adruino(1) #retrive a DICTIONARY of news in news type,5/6 as paramerter should be enough
        news_list.update(tmp_list)
    return tmp_list



def main():
    arduinoBlog_news = arduinoBlog()#now news_list have the arduino news in it
    input("All the news have been downloaded, moving on the selection.")

    #news_list=retobj.remove_coworker(news_list)#now news_list doesn't have the coworker news



    #news_list=ji.read_news(datafile,2)

    chosen_news = interface.start_interface(news_list,0)
    chosen_news = interface.start_interface(chosen_news,1)
    interface.clearConsole()
    print('\n\n')

    dev.main(chosen_news)




if __name__ == '__main__':
    interface.clearConsole()
    #print('Starting the code\n\n')
    main()
    #print("\n\nEverything seems to be working, if it doesen't it's because you're stupid.")
